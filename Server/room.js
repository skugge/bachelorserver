/**
 * This is the Room object (class)
 * @param wsMobileUser
 * @param wsPlayer
 * @constructor
 */
function Room(wsMobileUser,wsPlayer){

    this.wsMobileUser = wsMobileUser;
    this.wsPlayer = wsPlayer;

    this.getWsMobileUser = function(callback){
        callback(this.wsMobileUser);
    };

    this.getWsPlayer = function(callback){
        callback(this.wsPlayer);
    };

    this.sendMessage = function(sender,identifier,data,msg,callback){
        if(sender === "user"){
            //Message to the Player.
            message(this.wsPlayer,msg,function(res){
                callback(res);
            });
        }else if(sender === "player"){
            //Message to the user.
            message(this.wsMobileUser,msg,function(res){
                callback(res);
            });
        }
    };

    this.sendMessageToBoth = function(msg){
        message(this.wsPlayer,msg,function(res){});
        message(this.wsMobileUser,msg,function(res){});
    };

    //Local "Method"
    var message = function (receiverWS,msg,callback){
        try{
            receiverWS.send(msg);
            callback(true);
        }catch (err){
            receiverWS.send("couldNotSendMessage!");
            callback(false);
            console.log("Could not send message in room!");
        }
    };
}

module.exports = Room;