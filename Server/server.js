const https = require('https');
const fs = require('fs');
const WebSocket = require('ws');
const Db        = require('./db.js');
const Auth      = require('./auth.js');
const MHandler  = require('./messageHandler.js');
const CHandler  = require('./clientHandler.js');
const DBAuth    = require('./dbAuth.js');
const DbAsync   = require('./dbAsync.js');


const privateKey  = fs.readFileSync('./certificates/selfsigned.key', 'utf8');
const certificate = fs.readFileSync('./certificates/selfsigned.crt', 'utf8');
const options = {key: privateKey, cert: certificate};

const db = new Db();
const auth = new Auth();
const mh = new MHandler();
const ch = new CHandler();
const dbAuth = new DBAuth();
const dbAsync = new DbAsync();

//The array where active rooms are stored.
var rooms = [];


/**
 * The server
 */
const server = https.createServer(options);

/**
 * The upgrade too webSocket!
 */
const wss = new WebSocket.Server({server});


/**
 * The HTTPS server init.
 * It's connections are being upgraded to webSocket
 */
server.listen(3000, function listening () {
    console.log("Server active on port: "+server.address().port);
});


/**
 * This ON function runs on client webSocket connection.
 * And it is the only "reacting" function on messages, closure, error and such functions defined in the webSocket implementation.
 */
wss.on('connection', function connection (ws) {
    //Giving the db object to dbAuth:
    ws.identified = false;
    ws.room = false;
    console.log("User connected!");


    /**
     * This is the onMessage function, the message-handling function has for separation and readability reasons bean moved to the MessageHandler class.
     */
    ws.on('message', function message (msg) {
        mh.message(msg,ws,ch,auth,db,dbAuth,rooms,ch,dbAsync);
    });


    /**
     * OnClose runs on a client's webSocket disconnecting. All cleanup and logic bound to the disconnect of a webSocket is in this function.
     */
    ws.on('close', function close(){
        console.log("Closing room!");
        if(ws.room === true){
            try {
                mh.findRoomByWs(rooms,ws,function(room){
                    console.log("trying to send room:false message!");
                    if(ws.type === "user"){
                        room.wsPlayer.send("room:false");
                    }else if(ws.type === "player"){
                        room.wsMobileUser.send("room:false");
                    }
                    removeRoom(rooms,room,function(res){
                        console.log("did i remove the room? "+res);
                    });
                    console.log("Deleted room = "+room);
                });
            }catch (err){
                console.log(err);
            }
        }
        if (ws.type === "player"){
            console.log("WS of type:"+ws.type+ " with id:"+ws.dbid+" just CLOSED!");
            dbAuth.playerLogout(ws,db,ch,function () {
                console.log("Logged OUT!");
                ch.getAllPlayerIds(function(res){
                    mh.emit(ch.mobileUserClients,"buses",res);
                });
            });
            //send feedback to all mobileUsers that the player is offline
        }else if (ws.type === "user"){
            console.log("WS of type:"+ws.type+ " with id:"+ws.dbid+" just CLOSED!");
            dbAuth.mobileLogout(ws,db,ch);
        }else{
            try{
                console.log(ws.type+ " WS socket with id:"+ws.dbid+" just CLOSED!")
            }catch (Exception){
                console.log("ERROR: WS of unknown type just CLOSED!");
            }
        }
        ws.close()
    });

    /**
     * Like the name states, this is the the function reacting onError.
     * Errors happening on *this webSocket connection wil result in a cast here.
     */
    ws.on('error', () => function(error){
        console.log("WebSocket Error: ");
        console.log(error);
        //ws.close();
    });
});


//Help methods

/**
 * This is a cleanup process for when a room connection is broken.
 * It's main objective is to set the room variables for the individual room inhabitants to false,
 * and remove the @room object from the array of rooms.
 * @param rooms     The Array of all active rooms.
 * @param room      The room for removal.
 * @param callback  The callback function.
 */
var removeRoom = function(rooms,room,callback){
    length = rooms.length;
    index = rooms.indexOf(room);
    if(index !== null){
        try {
            rooms.splice(index);
            dbAsync.removeRoom(room.wsMobileUser.dbid,room.wsPlayer.dbid,db);
            room.wsMobileUser.room = false;
            room.wsPlayer.room     = false;
            room = null;
            console.log("Length after splice: "+rooms.length+" is the tab one shorter? ");
            callback((length === (rooms.length+1)));
            //Send feedback to all active users!
        }catch (err){
            console.log("Error in trying to remove room:"+err);
            callback(false);
        }
    }
};