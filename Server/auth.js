crypto = require('crypto');

/**
 * Influenced by the following:
 * https://ciphertrick.com/2016/01/18/salt-hash-passwords-using-nodejs-crypto/
 */


function Auth(){
    /**
     * Private pbkdf2 function used to hash and salt and iterate the the secret.
     * @param secret    The secret, usually the password, that needs to be hashed with given salt.
     * @param salt      The salt.
     * @param callback  The callback function
     */
    var pbkdf2 = function(secret, salt, callback){
        crypto.pbkdf2(secret, salt,1000,64, 'sha512',function(err,buffer){
            if(err){
                callback(null)
            }else {
                console.log(buffer.toString('hex'));
                callback(buffer.toString('hex'));
            }

        });
    };

    /**
     * Token generator method,
     * used to generate a sett of random bytes to compare on auto login by user.
     * Reason for this is so one does not have to save the password on the phone.
     * @param callback  Callback function
     */
    var generateToken = function(callback){
        callback(crypto.randomBytes(Math.ceil(128/2)).toString('hex').slice(0,128));
    };

    /**
     * Just a simple hash function made a public function.
     * @param secret  The string needing to be hashed.
     * @param callback The callback function.
     */
    this.hashPassword = function(secret,callback){
        pbkdf2(secret,"salt",function(res){
            callback(res);
        });
    };

    /**
     * This function is part of the authentication-process a mobile-client has to pass in order to access a websocket-connection.
     * In more detail it "hash and salt" then compares with the data gathered from the database and returns boolean value.
     * @param username      The Username.
     * @param pwd           The password.
     * @param salt          The salt from database.
     * @param usernameH     The hashed-password from database.
     * @param pwdH          The hashed-salt-password from database.
     * @param token         Token from db to compare the pwd against.
     * @param callback      Callback function
     * @returns {boolean}   The compare result.
     */
    this.mobileUserValidate = function(username, pwd, salt, usernameH, pwdH,token,callback){
        this.hashPassword(username,function(hashedUsername){
            if (hashedUsername === usernameH){
                pbkdf2(pwd,salt,function(hashedPwd){
                    //console.log("Pwd:"+pwd);
                    //console.log("Token: "+token);
                    if((hashedPwd === pwdH)||(pwd === token)){
                        //generate and return token.
                        console.log("Compared successfully!");
                        generateToken(function(res){
                            //console.log(res);
                            callback(res);
                        });
                    }else{
                        console.log("Compared unsuccessfully!");
                        callback(null);
                    }
                });
            }else{
                callback(null);
            }
        });
    }
}



module.exports = Auth;