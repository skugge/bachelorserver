/**
 * The database Authentication class (object)
 * Abstraction for Simplification purposes.
 * OBS!! Not necessary relaying on being a class (object)!
 */
function  DbAuth(){

    /**
     * This function is part of the authentication-process for a mobile-client to access the websocket-connection.
     *
     * @param username  The username passed from the mobile-client for identification and verification purposes.
     * @param password  The hashed-password passed from the mobile-client to verify against the database.
     * @param db        The Database object, called on communication with the database.
     * @param auth      The authentication object, makes it posible to access it's verification and cryptographic-hashfunction methods.
     * @param callback  The callback-function, makes the function caller wait (async) for this function to respond.
     */
    this.mobileLogin = function(username, password, db, auth, callback){
        auth.hashPassword(username,function (hashRes){
            if(hashRes){
                db.sql_query('SELECT * FROM `User` WHERE `username` LIKE "'+hashRes+'"',function(user){
                    if(user){
                        if (user.length > 0){
                            auth.mobileUserValidate(username,password,user[0].salt,user[0].username,user[0].pwd,user[0].token,function(token) {
                                if (!token) {
                                    console.log("Failed to authenticate user with ether token or password!");
                                    callback(null);

                                    console.log("Removing Token from database!");
                                    db.sql_query('UPDATE `User` SET `token` = "" WHERE `User`.`idUser` = "'+user[0].idUser+'";', function (res) {
                                        if (res){
                                            console.log("Successfully deleted the db token!");
                                        }else{
                                            console.log("Unsuccessfully tried to delete the db token!")
                                        }
                                    });
                                } else {
                                    callback(user[0].idUser,token);

                                    // Sending new token to db;
                                    console.log("The token sendt to db.");
                                    db.sql_query('UPDATE `User` SET `token` = "' + token + '" WHERE `User`.`idUser` = "'+user[0].idUser+'";', function (res) {
                                        if (res){
                                            console.log("Successfully updated the db token!");
                                        }else{
                                            console.log("Unsuccessfully tried to updated the db token!")
                                        }
                                    });

                                    //update db to active
                                    db.sql_query('UPDATE `User` SET `active` = "1" WHERE `User`.`idUser` = "'+user[0].idUser+'";',function(res){
                                        if(res){
                                            console.log("Successfully updated the db to active!");
                                        }else{
                                            console.log("unsuccessfully tried to updated the db to active!")
                                        }
                                    });
                                }
                            });
                        }else{
                            console.log("Non-Excising user!");
                            callback(null);
                        }
                    }else{
                        callback(null);
                    }
                })
            }
        });
    };

    /**
     * This is part of the authentication-process for a player-device to be allowed to access the websocket-connection.
     * Here the database-query's are being handled and the result returned to the function caller.
     * @param token     The Token to identify a player device. This Token should be explicit to a single device, and thereby have a low probability of collision.
     * @param db        The Database object, called on communication with the database.
     * @param callback  The callback-function, makes the function caller wait (async) for this function to respond with data.
     */
    this.playerLogin = function(token, db, callback){
        db.sql_query("SELECT * FROM `Bus` WHERE `token` LIKE '"+token+"'",function (res){
            if (res && res.length > 0){
                console.log(res);
                console.log("The id i found: "+res[0].idBus);
                db.sql_query("UPDATE `Bus` SET `active` = '1' WHERE `Bus`.`idBus` = '"+res[0].idBus+"';",function (res){
                    if (res) {
                        console.log("Successfully updated the db to active!");
                    }
                });
                if(res[0].token === token){
                    console.log(res[0].token+"    "+token);
                    callback(res[0].idBus)
                }
            }else{
                console.log("First time connecting?");
                console.log("NonExcisting user, create new!");
                db.sql_query("INSERT INTO `Bus` (`token`,`active`) VALUES ('"+token+"',1)",function (res){
                    if(res){
                        console.log(res.insertId);
                        callback(res.insertId);
                    }else{
                        callback(null);
                    }
                });
            }
        });
    };


    /**
     * Login function for the webserver;
     * @param token     The Token.
     * @param db        The Database object, called on communication with the database.
     * @param callback  The callback-function, makes the function caller wait (async) for this function to respond with data.
     */
    this.serverLogin = function(token,db,callback){
        db.sql_query('SELECT * FROM `ServerToken`;',function (res) {
            //console.log(token+"  ===? "+res[0].token);
            if(token === res[0].token){
                callback(true);
                db.sql_query("UPDATE `ServerToken` SET `token` = '"+crypto.randomBytes(Math.ceil(64/2)).toString('hex').slice(0,64)+"' WHERE `ServerToken`.`idServerToken` = 1;",function (res){
                    //console.log(res);
                });
            }else{
                callback(false);
            }
        })
    };


    /**
     * This is the function cleaning up when a mobile-client's websocket disconnects. Also better known as the logout function.
     * @param dbid  The Id given the websocket on login corresponding to it's primary key in the Database.
     * @param db    The Database object, called on communication with the database.
     * @param ch    The Client Handler object, all active client based websocket-connections are stored here.
     */
    this.mobileLogout = function (ws, db, ch){
        db.sql_query('UPDATE `User` SET `active` = "0" WHERE `User`.`idUser` = "'+ws.dbid+'";',function(res){
            if(res){
                console.log("Successfully updated the db to Deactive!");
                //ch.mobileUserClients.pop();
                ch.removeMobileUser(ws);
            }else{
                console.log("UNSuccessfully tried to updated the db to Deactive!");
            }
        })
    };


    /**
     * This is the function cleaning up when a player-client's websocket disconnects. Also better known as the logout function.
     * @param dbid  The Id given the websocket on login corresponding to it's primary key in the Database.
     * @param db    The Database object, called on communication with the database.
     * @param ch    The Client Handler object, all active client based websocket-connections are stored here.
     */
    this.playerLogout = function (ws, db, ch,callback){
        db.sql_query('UPDATE `Bus` SET `active` = 0 WHERE `Bus`.`idBus` = "'+ws.dbid+'";',function(res){
            if(res){
                console.log("Successfully updated the db to Deactive!");

                ch.removePlayerClients(ws,function () {
                    callback()
                });
            }
        })
    };
}


module.exports  = DbAuth;