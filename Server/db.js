var fs = require("fs");
var mysql = require('mysql');
var async = require('async');
var path = require('path');


/**
 * The Database manager class (object)
 * This class is called on communication with the database.
 * It keeps the mysql connection-pool and handles all sql-querys.
 */
function Db(){
    var filePath = path.join(__dirname, './certificates/dbCredentials.txt');
    var str = fs.readFileSync(filePath, {encoding: 'utf-8'}).toString();
    str = str.split(',');
    var pool;

    /**
     * The init function
     * Initiates the connection-pool.
     */
    this.init = function(){
        pool  = mysql.createPool({
            connectionLimit : 10,
            host            : str[2],
            user            : str[0],
            password        : str[1],
            database        : str[0]
        });
    };

    /**
     * The Sql-query handler, its job is to get a connection from the connection-pool, ask the database, and return the answer through a callback.
     *
     * @param sql       The Sql-query in string-format.
     * @param callback  The callback-function, makes the function caller wait for this function to respond with data.
     */
    this.sql_query = function(sql, callback){
        pool.getConnection(function(err, connection) {
            // Use the connection
            connection.query(sql, function (error, results, fields) {
                // And done with the connection.
                connection.release();
                // Handle error after the release.
                if (error){
                    console.log(error);
                    callback(null);
                }else{
                    callback(results);
                }
            });
        });
    };
    this.init();
}



module.exports = Db;