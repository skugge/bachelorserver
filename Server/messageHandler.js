var async = require('async');
var Room = require('./room.js');

/**
 * The Message Handler
 * A large part of the post connection authentication logic gets handled here,
 * also the entire Socket onMessage gets restricted and controlled from this class.
 *
 * The main purpose of this class though remains to control the messaging flow between webSocket-connected clients.
 */
function MessageHandler(){

    /**
     * A search function that takes a websocket-connection as parameter and returns a room if it contains the given connection.
     * @param rooms     The Room array
     * @param ws        The websocket-connection object
     * @param callback  The callback-function, makes the function caller wait (async) for this function to respond with data.
     */
    var findRoomByWS = function(rooms,ws,callback){
        console.log("trying to find room in findroombyWS! tablenght: "+rooms.length);
        rooms.forEach(function (element) {
            if(element.wsPlayer === ws || element.wsMobileUser === ws){
                callback(element);
            }
        });
    };

    /**
     * Public version of method above!
     * @param rooms     The Room array
     * @param ws        The webSocket-connection object
     * @param callback  The callback-function, makes the function caller wait (async) for this function to respond with data.
     */
    this.findRoomByWs = function(rooms,ws,callback){
        findRoomByWS(rooms,ws,function (res) {
            callback(res);
        })
    };


    var createRoom = function(db,dbAuth,ch,id,ws,rooms,dbAsync){
        ch.getPlayerClientById(id, function(playerWs){
            if(playerWs !== false){
                ws.room = true;
                playerWs.room = true;
                var newRoom = new Room(ws,playerWs);
                rooms.push(newRoom);
                console.log("Sendt to newRoom: "+ws.dbid+"   "+playerWs.dbid)
                dbAsync.newRoom(ws.dbid,playerWs.dbid,db);
                newRoom.sendMessageToBoth("room:true");
            }else{
                ws.send("room:false");
                console.log("There was a error on return from getPlayerclient!");
            }
        });
    };

    var emit = function (list,msg,data) {
        list.forEach(function (element) {
            element.send(msg+":"+data);
        });
    };

    this.emit = function(list,msg,data){
        emit(list,msg,data);
    };

    /**
     * The main Messagehandling method
     * Each webSocket-connections messages pass through this function.
     * It first runs the allowed function which determines if it's connection is authorized, if not does so.
     * After it's sure its connection is valid the message is passed to the then-function.
     */
    this.message = function(msg,ws,clientHandler,auth,db,dbAuth,rooms,ch,dbAsync){
        /**
         * The Allowed-function
         * This is the the Method which determines if a connection is valid before passing it to the then-function.
         *
         * @param msg   The entire message passed by webSocket client
         * @param ws    The connection object.
         * @returns {Promise<any>}  Returns the promise to make sure it runs before the then-function.
         */
        function allowed(msg,ws) {
            return new Promise(resolve => {
                if (!ws.identified){
                    msg = msg.toString();
                    var str = msg.split(":");
                    console.log(str);
                    if (msg.substring(0,3) === "ima" && str.length > 1){
                        if (str[0] === "imaUser" && str.length > 2){
                            console.log("password or token! :"+str[2]);
                            dbAuth.mobileLogin(str[1], str[2], db, auth, function(res, newtoken){
                                if (res){
                                    console.log("USER: "+res+" successfully connected!");
                                    ws.dbid = res;
                                    ws.type = "user";
                                    console.log("Authenticate message sendt to client!");
                                    console.log("authenticated:true:"+newtoken);
                                    ws.send("authenticated:true:"+newtoken);
                                    clientHandler.addMobileUser(ws);
                                    resolve(ws.identified = true);
                                }else{
                                    console.log("USER Authentication failed!");
                                    ws.send("authenticated:false");
                                    resolve(false);
                                }
                            })
                        }else if(str[0] === "imaPlayer" && str.length > 1 && str[1].length >= 64){
                            dbAuth.playerLogin(str[1],db,function(res){
                                if (res){
                                    console.log("Player: "+res+" successfully connected!");
                                    ws.dbid = res;
                                    ws.type = "player";
                                    console.log("authenticated:true");
                                    ws.send("authenticated:true");
                                    clientHandler.addPlayer(ws);
                                    resolve(ws.identified = true);
                                }else{
                                    console.log("Player Authentication failed!");
                                    ws.send("authenticated:false");
                                    resolve(false);
                                }
                            })
                        }else if(str[0] === "imaWebserver" && str.length > 1 && str[1].length >= 64) {
                            dbAuth.serverLogin(str[1], db, function (res) {
                                if (res) {
                                    console.log("Webserver Authentication Success!");
                                    clientHandler.addServer(ws);
                                    resolve(ws.identified = true);
                                } else {
                                    console.log("Webserver Authentication failed!");
                                    resolve(false);
                                }
                            })
                        }else{
                            console.log("Wrong token length!  string length: "+str[1].length);
                            resolve(false)
                        }
                    }else{
                        console.log("Fail! Message formation error.  line * MessageHandler.js");
                        resolve(false);
                    }
                }else if (ws.identified){
                    console.log("resolved true!");
                    resolve(true);
                }else{
                    console.log("NOT Authorized!!!");
                    resolve(false);
                }
            });
        }

        /**
         * The then-function
         *  This is the heart of this code, it's job is to make sure all the messages being allowed follow the given syntax and then handles them if not passing them to sub management.
         *  For instance the "in room" messages are being passed directly though the room class.
         *  The message syntax is as follows:
         *      "message:data:data:data:....:..." and so forth.
         * @param msg   The message.
         * @param ws    the webSocket object.
         * @returns {Promise<void>}
         */
        async function then(msg,ws) {
            console.log("Before auth check! " + ws.identified+" of type: "+ws.type+" room: "+ws.room);
            if(await allowed(msg,ws)){
                console.log("After auth check! " + ws.identified+" of type: "+ws.type+" room: "+ws.room);
                async.waterfall([
                    function (callback) {
                        var data = msg.split(":");
                        var identifier = data[0];
                        data[0] = "";
                        callback(null, identifier, data);
                    }
                ], function (err, identifier, data) {
                    switch (identifier) {
                        case 'imaUser':
                            // User after authentication
                            console.log("After Auth! User!");
                            ch.getAllPlayerIds(function(res){
                                //Sending all players to this webSocket-connection:
                                ws.send("buses:"+res);
                            });
                            break;
                        case 'imaPlayer':
                            // Player after authentication:
                            console.log("After Auth! Player!");
                            ch.getAllPlayerIds(function(res){
                                //Sending all players to this webSocket-connection:
                                emit(ch.mobileUserClients,"buses",res);
                            });
                            break;
                        default:
                            if(ws.type === "player"){
                                //specific case for ws.type = player
                                if(ws.room === false){
                                    console.log("As a Player you are NOT allowed to send a message before in a room. Player with dbid: "+ws.dbid+" tried to send identifier:"+identifier+" with following data: " + data);
                                }else{
                                    console.log("Player: "+ws.dbid+" has sent a message directly to the user in its room!"+msg);
                                    findRoomByWS(rooms,ws,function(room){
                                        room.sendMessage(ws.type,identifier,data,msg,function(res){});
                                        console.log("This is the room i found"+room);
                                    });
                                }
                            }else if (ws.type === "user"){
                                //specific case for ws.type = user
                                if(ws.room === false){
                                    switch (identifier) {
                                        case "createRoom":
                                            console.log("Creating new room with UserID: "+ws.dbid+"  and PlayerID:"+data[1]);
                                            createRoom(db,dbAuth,ch,data[1],ws,rooms,dbAsync);
                                            break;
                                        default:
                                            console.log("Unknown Identifier for user: " + identifier + " with following data: " + data);
                                            break;
                                    }
                                }else{
                                    console.log("User: "+ws.dbid+" has sent a message directly to the player in its room!"+msg);
                                    findRoomByWS(rooms,ws,function(room){
                                        room.sendMessage(ws.type,identifier,data,msg,function(res){});
                                        console.log("This is the room i found"+room);
                                    });
                                }
                            }
                    }
                });
            }else{
                ws.close()
            }
        }

        then(msg,ws);
    };
}



module.exports = MessageHandler;