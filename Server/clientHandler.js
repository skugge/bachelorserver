/**
 * The ClientHandler
 * The clienthandler is meant to keep all active client-based webSocket-connections stored as objects.
 *
 * @param playerClients         This is an array that keeps all active webSocket-connected player clients stored.
 * @param mobileUserClients     This is an array that keeps all active webSocket-connected mobile user clients stored.
 * @constructor
 */
function ClientHandler(){
    this.playerClients = [];
    this.mobileUserClients = [];
    this.serverClient=[];

    /**
     * Pushes a new mobile user client webSocket-connection into the mobileUserClients array.
     * @param ws    The webSocket-connection object.
     */
    this.addMobileUser = function (ws){
        this.mobileUserClients.push(ws);
    };

    /**
     * Pushes a new player client webSocket-connection into the playerClients array.
     * @param ws    The webSocket-connection object.
     */
    this.addPlayer = function (ws){
        this.playerClients.push(ws);
    };

    /**
     * Pushes the webservers client webSocket-connection into the serverClient array.
     * @param ws    The webSocket-connection object.
     */
    this.addServer = function (ws){
        this.serverClient.push(ws);
    };

    /**
     * Removes a given player from the server list @this.playerClients
     * @param ws    The webSocket-connection object.
     */
    this.removePlayerClients = function(ws,callback){
        const index = this.playerClients.indexOf(ws);
        if(ws.dbid === this.playerClients[index].dbid){
            this.playerClients.splice(index, 1);
            callback()
        }
    };

    /**
     * Removes a given player from the server list @this.mobileUserClients
     * @param ws    The webSocket-connection object.
     */
    this.removeMobileUser = function(ws){
        const index = this.mobileUserClients.indexOf(ws);
        if(ws.dbid === this.mobileUserClients[index].dbid){
            this.mobileUserClients.splice(index, 1);
        }
    };

    /**
     * Removes a given webServer from the server list @this.serverClient
     * @param ws    The webSocket-connection object.
     */
    this.removeServer = function(ws){
        const index = this.serverClient.indexOf(ws);
        if(ws === this.serverClient[index]){
            this.serverClient.splice(index, 1);
        }
    };

    /**
     * This is a search function returning, through a callback-function, the player webSocket-connection object corresponding to given id.
     * @param playerId      The ID.
     * @param callback      The callback-function, makes the function caller wait (async) for this function to respond with data.
     */
    this.getPlayerClientById = function(playerId, callback){
        for(var i = 0; i < this.playerClients.length; i += 1){
            if((this.playerClients[i].dbid === parseInt(playerId))) {
                callback(this.playerClients[i]);
            }
        }
    };

    /**
     * Returns all webSocket-connected players:
     * @param callback
     */
    this.getAllPlayerIds = function (callback) {
        var tab = [];
        var tab2 = this.playerClients;

        function first(tab2) {
            return new Promise(resolve => {
                tab2.forEach(function (element) {
                    tab.push(element.dbid);
                },resolve(tab));
            });
        }
        async function then(tab2) {
            callback(await first(tab2));

        }
        then(tab2);
    }

}

module.exports = ClientHandler;